//
//  ContentView.swift
//  dogs-breed
//
//  Created by Hannes Dermutz on 04.12.19.
//  Copyright © 2019 Hannes Dermutz. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    
    @ObservedObject var networkManager = NetworkManager()
    let connection = NetworkConnectionChecker()
    @FetchRequest(
        entity: Breed.entity(),
        sortDescriptors: [NSSortDescriptor(keyPath: \Breed.name, ascending: true)]
    ) var breeds: FetchedResults<Breed>
    var body: some View {
        NavigationView {
            List(breeds) { (breed:Breed) in
                NavigationLink(destination: DetailView(breed: breed, breedImages: self.networkManager.prepareFetchRequestForDetailView(breed: breed), pickedImage: nil)) {
                        Text(breed.name!)
                    }
        
            }
        .navigationBarTitle("Dog Breeds App 🐶")
        }
        .onAppear {
            if self.breeds.isEmpty == true {
                self.networkManager.fetchData()
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}

