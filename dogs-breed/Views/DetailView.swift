//
//  DetailView.swift
//  dogs-breed
//
//  Created by Hannes Dermutz on 04.12.19.
//  Copyright © 2019 Hannes Dermutz. All rights reserved.
//

import SwiftUI

struct DetailView: View {
    
    let breed: Breed
    var breedImages: [BreedImage]
    let connection = NetworkConnectionChecker()
    @State var pickedImage: BreedImage?
    
    @ObservedObject var networkManager = NetworkManager()
    
    var body: some View {
        VStack {
            VStack {
                Text((self.connection.isConnectedToNetwork() == false ? "Currently no Internet connection. To fetch new Images please connect your phone to the internet." : ""))
                Text(breed.name!)
                    .font(.system(size: CGFloat(integerLiteral: 24)))
                    .fontWeight(.bold)
                Image(uiImage: (pickedImage != nil ? UIImage(data: (pickedImage?.data!)!)! : (breedImages.isEmpty == false ? UIImage(data: (breedImages.randomElement()?.data!)!)! : UIImage())))
                .resizable()
                .scaledToFit()
                .frame(minWidth: CGFloat(0), maxWidth: .infinity, minHeight: CGFloat(0), maxHeight: .infinity, alignment: .center)
                
                Button(action: {self.networkManager.fetchCorrespondingImage(breed: self.breed); self.viewNeedsRefresh()}) {
                    Text(self.connection.isConnectedToNetwork() == false ? "Fetch local Image" : "Fetch new Image")
                        .padding()
                        .background(Color.blue)
                        .foregroundColor(Color.white)
                        .cornerRadius(CGFloat(2.5))
                }
                .padding(.bottom, CGFloat(40))
            
            
            }
            .onAppear(){
                self.networkManager.fetchCorrespondingImage(breed: self.breed) 
            }
            
        }
    }
    
    func viewNeedsRefresh() {   
        if self.breedImages.count > 1 {
                
            let randomImage = self.breedImages.randomElement()!
            self.pickedImage = randomImage
            
            var currentImage = self.pickedImage
            
            while currentImage?.image_name == randomImage.image_name {
                currentImage = self.breedImages.randomElement()
            }
            
            //here seems to be a SwiftUI bug: normally the pickedImage is updated and the View is redrawn. Mostly its working but not always.
            self.pickedImage = currentImage
        }
    }
}


struct DetailView_Previews: PreviewProvider {
    static var previews: some View {
        DetailView(breed: Breed(), breedImages: [BreedImage](), pickedImage: BreedImage())
    }
}


