//
//  BreedData.swift
//  dogs-breed
//
//  Created by Hannes Dermutz on 04.12.19.
//  Copyright © 2019 Hannes Dermutz. All rights reserved.
//

import Foundation

struct Results: Decodable {
    let message: [String : [String?]]
}

//make Breed Identifiable so its possible to iterate over it with SwiftUI
extension Breed: Identifiable {
    
}
