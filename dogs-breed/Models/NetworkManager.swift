//
//  NetworkManager.swift
//  dogs-breed
//
//  Created by Hannes Dermutz on 04.12.19.
//  Copyright © 2019 Hannes Dermutz. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class NetworkManager: ObservableObject {
    
    @Published var breeds = [Breed]()
    @Published var breedImage = Data()
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func fetchData() {
        if let url = URL(string: "https://dog.ceo/api/breeds/list/all") {
            
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (data, response, error) in
                if error == nil {
                    let decoder = JSONDecoder()
                    if let safeData = data {
                        do {
                            let results = try decoder.decode(Results.self, from: safeData)
                            var rawBreeds = [Breed]()
                            
                            results.message.forEach { (arg0) in
                                let (breedName, subBreeds) = arg0
                                
                                let breed = Breed(context: self.context)
                                breed.id = breedName
                                breed.name = breedName.capitalizeFirstLetter()
                                
                                rawBreeds.append(breed)
                                
                                //add each sub-breed to breeds
                                if subBreeds.isEmpty == false {
                                    for subBreed in subBreeds {
                                        if let subBreed = subBreed {
                                            let id = "\(breedName)-\(subBreed)"
                                            let name = "\(subBreed.capitalizeFirstLetter()) \(breedName.capitalizeFirstLetter())"
                                            let breed = Breed(context: self.context)
                                            breed.id = id
                                            breed.name = name
                                            rawBreeds.append(breed)
                                        }
                                    }
                                }
                            }
                            try self.context.save()
                        } catch {
                            print(error)
                        }
                    }
                }
            }
            task.resume()
        }
    }
    
    
    func fetchCorrespondingImage(breed: Breed){
        let breedName = breed.id!.replacingOccurrences(of: "-", with: "/")
        
        if let url = URL(string: "https://dog.ceo/api/breed/\(breedName)/images/random") {
            let session = URLSession(configuration: .default)
            let task = session.dataTask(with: url) { (data, response, error) in
                if error == nil {
                    if let safeData = data {
                        do {
                            let result = try JSONSerialization.jsonObject(with: safeData, options: []) as? [String: String]
                            
                            let resultImageUrl = result!["message"]
                            
                            if let imageUrl = URL(string: resultImageUrl!) {
                                do {
                                   
                                    //Check if fetched image already exists
                                    if self.checkIfImageExists(imageName: imageUrl.lastPathComponent) == false {
                                        let breedImage = BreedImage(context: self.context)
                                        var breedImageData = Data()
                                        
                                        breedImageData = try Data(contentsOf: imageUrl)
                                        breedImage.breed = breed
                                        breedImage.breed_id = breed.id!
                                        breedImage.data = breedImageData
                                        breedImage.image_name = imageUrl.lastPathComponent
                                        
                                        try self.context.save()
                                        
                                    }
                                }catch {
                                    print(error)
                                }
                            }
                        }catch {
                            print(error)
                        }
                    }
                }
            }
            task.resume()
            
        }
    }
    
    
    func checkIfImageExists(imageName: String) -> Bool {
        let request: NSFetchRequest<BreedImage> = BreedImage.fetchRequest()
        request.predicate = NSPredicate(format: "image_name == %@", imageName)
        
        do {
            let results = try context.fetch(request)
            print(results)
            
            if results.isEmpty == true {
                return false
            }else {
                print("Duplicate found. Not saving to db.")
                return true
            }
            
        } catch {
            print("Error occured while checking if Image exists in database: \(error)")
            return true
        }
    }
    
    func prepareFetchRequestForDetailView(breed: Breed) -> [BreedImage] {
        let request: NSFetchRequest<BreedImage> = BreedImage.fetchRequest()
        request.predicate = NSPredicate(format: "breed_id == %@", breed.id!)
        
        var array = [BreedImage]()
        do {
            array = try context.fetch(request)
            
        }catch {
            print(error)
        }
        
        return array
    }
}

extension String {
    func capitalizeFirstLetter() -> String {
        return prefix(1).uppercased() + self.lowercased().dropFirst()
    }
}
